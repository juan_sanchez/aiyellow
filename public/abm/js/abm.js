var baseUrl = GetBaseUrl();

function coursedatesPaginatorAjax( page ){
	var id_course = $('#id_course').val();
	$.post(this.baseUrl+'/admin/coursedatepaginator', {id_course : id_course, page: page},function(data) {
		$('#datesContainer').html(data);	
	});
}

function deleteDate( id ){
	answer = confirm ("Are you sure you want to delete this date?")
	if (answer){
		$.post(this.baseUrl+'/admin/datesdeletajax', {id : id},function(data) {
			coursedatesPaginatorAjax(1);
		}, 'json');
	}
}

function inscriptionPaginatorAjax( page ){
	var id_course = $('#id_course').val();
	$.post(this.baseUrl+'/admin/inscriptionpaginator', {id_course : id_course, page: page},function(data) {
		$('#inscriptionContainer').html(data);	
	});
}

function deleteInscription( id ){
	answer = confirm ("Are you sure you want to delete this inscription?")
	if (answer){
		$.post(this.baseUrl+'/admin/inscriptiondeleteajax', {id : id},function(data) {
			inscriptionPaginatorAjax(1);
		}, 'json');
	}
}

function GetBaseUrl() {
	var l = window.location;
	var base_url = l.protocol + "//" + l.host;
	return base_url;
}
