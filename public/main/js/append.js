var baseUrl = GetBaseUrl();

function graduatePaginatorAjax( page, language ){
	$.post(this.baseUrl+'/public/graduatepaginator', {page: page, language: language},function(data) {
		$('#graduateContainer').html(data);	
	});
}

function coursePaginatorAjax( page, language ){
	$.post(this.baseUrl+'/public/coursepaginator', {page: page, language: language},function(data) {
		$('#courseContainer').html(data);	
	});
}

function eventPaginatorAjax( page, language ){
	$.post(this.baseUrl+'/public/eventpaginator', {page: page, language: language},function(data) {
		$('#eventContainer').html(data);	
	});
}

function inscriptionModalAjax( id_course, language ){
	$.post(this.baseUrl+'/public/inscriptionmodal', {id_course: id_course, language: language},function(data) {
		$('#myModal').html(data);	
		$("#myModal").modal();    
	});
}

function testimonyPaginatorAjax( page, language ){
	var country = $('#country').val();
	$.post(this.baseUrl+'/public/testimonypaginator', {page: page, country: country, language: language},function(data) {
		$('#testimonyContainer').html(data);	
	});
}


function submitInscriptionForm(){
	var id_course = $('#id_course').val();
	var name 	  = $('#name').val();
	var lastname  = $('#lastname').val();
	var email	  = $('#email').val();
	var date	  = $('#date').val();
	var error 	  = false;
	
	if(name == ''){ error = true; $('#name').addClass('errorMsg'); } else { $('#name').removeClass('errorMsg'); }
	if(lastname == ''){ error = true; $('#lastname').addClass('errorMsg'); } else { $('#lastname').removeClass('errorMsg'); }
	if(!isValidEmailAddress(email)){ error = true; $('#email').addClass('errorMsg'); } else { $('#email').removeClass('errorMsg'); }
	
	if(error){
		$('#success').addClass('oculto');
		$('#error').removeClass('oculto');
	}else{
		$.post(this.baseUrl+'/public/submitinscriptionajax', {id_course: id_course, name: name, lastname: lastname, email: email, date: date},
			function(status) {
				if(status){
					$('#systemerror').addClass('oculto');
					$('#error').addClass('oculto');
					$('#success').removeClass('oculto');
					$('submitButton').attr('onclick','javascript:$("#myModal").closemodal();')
				}else{
					$('#success').addClass('oculto');			
					$('#systemerror').removeClass('oculto');
				}
			}, 'json'
		);
	}

}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};

function GetBaseUrl() {
	var l = window.location;
	var base_url = l.protocol + "//" + l.host;
	return base_url;
}
