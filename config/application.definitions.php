<?php

defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

/** PATH CONFIGURATION **/

chdir(dirname(__DIR__));

defined('BASE_PATH') || define( 'BASE_PATH', realpath(dirname(__DIR__)) );

defined('PICS_PATH') || define( 'COURSES_PICS_PATH', BASE_PATH . '/public/img_courses' );
defined('PICS_PATH') || define( 'EVENTS_PICS_PATH', BASE_PATH . '/public/img_events' );
defined('PICS_PATH') || define( 'GRADUATES_PICS_PATH', BASE_PATH . '/public/img_graduates' );

defined('MODULES_PATH') || define('MODULES_PATH', BASE_PATH . '/module' );

defined('RECAPTCHA_PUB') || define('RECAPTCHA_PUB', '6LdYE98SAAAAAFWspP2LokEcj0-pncRxVmszb4Oq' );
defined('RECAPTCHA_PRV') || define('RECAPTCHA_PUB', '6LdYE98SAAAAAJcUto0CR8M2-9qmQOCMy7SfsPyd' );

