<?php

return array(
    'controllers' => array(
        /** controllers aliases **/
        'invokables' => array(
            'admin'  => 'Acreditation\Controller\AdminController',
            'public' => 'Acreditation\Controller\PublicController',
        ),
    ),
    /** route definitions **/
    'router' => array(
        'routes' => array(
            /** employee admin route. no need translate **/
            'admin' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'admin',
                        'action'     => 'courselist',
                    ),
                ),
            ),
            'home' => array(
                'type' => 'literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'public',
                        'action'     => 'index',
                        'language'   => 'en'
                    ),
                ),
            ),
            /** public/index route. default route **/
            'public' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '[/:language]/public[/:action][/:id]',
                    'constraints' => array( 
                        'lang' => '[a-zA-Z]{2}' 
                    ),
                    'defaults' => array( 
                        // same controller and action, but now the language is set 
                        // from here on we're gonna create children that will all have the language set 
                        'controller' => 'public',
                        'action'     => 'index',
                        'language'   => 'en'
                    ), 
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    // every child will have the language-part in the url 
                    // so, /public[/][:action][/:id], really is /[:lang]/public[/][:action][/:id]
                    'public' => array(
                        'type'    => 'segment', 
                        'options' => array(
                            'route'    => '[/:action][/:id]' ,
                            'defaults' => array(
                                'controller' => 'public',
                                'action'     => 'index',
                                'language'   => 'en'
                            ), 
                        ), 
                    ), 
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'acreditation' => __DIR__ . '/../view',
        ), 
	    'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);
