<?php
namespace Acreditation;

// Add these import statements:
use Acreditation\Model\Course;
use Acreditation\Model\Coursedate;
use Acreditation\Model\Inscription;
use Acreditation\Model\Event;
use Acreditation\Model\Graduate;
use Acreditation\Model\Testimony;
use Acreditation\Model\Country;

use Acreditation\Model\CourseTable;
use Acreditation\Model\CoursedateTable;
use Acreditation\Model\InscriptionTable;
use Acreditation\Model\EventTable;
use Acreditation\Model\GraduateTable;
use Acreditation\Model\TestimonyTable;
use Acreditation\Model\CountryTable;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
    public function onBootstrap(\Zend\Mvc\MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $eventManager->attach(\Zend\Mvc\MvcEvent::EVENT_ROUTE, array($this, 'onLanguageRoute'));
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                ////////////////MODELS////////////////
                'Acreditation\Model\CourseTable' =>  function($sm) {
                    $tableGateway = $sm->get('CourseTableGateway');
                    $table = new CourseTable($tableGateway);
                    return $table;
                },
                'Acreditation\Model\CoursedateTable' =>  function($sm) {
                    $tableGateway = $sm->get('CoursedateTableGateway');
                    $table = new CoursedateTable($tableGateway);
                    return $table;
                },
                'Acreditation\Model\InscriptionTable' =>  function($sm) {
                    $tableGateway = $sm->get('InscriptionTableGateway');
                    $table = new InscriptionTable($tableGateway);
                    return $table;
                },
				'Acreditation\Model\EventTable' =>  function($sm) {
                    $tableGateway = $sm->get('EventTableGateway');
                    $table = new EventTable($tableGateway);
                    return $table;
                },
				'Acreditation\Model\GraduateTable' =>  function($sm) {
                    $tableGateway = $sm->get('GraduateTableGateway');
                    $table = new GraduateTable($tableGateway);
                    return $table;
                },
				'Acreditation\Model\TestimonyTable' =>  function($sm) {
                    $tableGateway = $sm->get('TestimonyTableGateway');
                    $table = new TestimonyTable($tableGateway);
                    return $table;
                },
				'Acreditation\Model\CountryTable' =>  function($sm) {
                    $tableGateway = $sm->get('CountryTableGateway');
                    $table = new CountryTable($tableGateway);
                    return $table;
                },
                
                ////////////////GATEWAYS////////////////
                'CourseTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Course());
                    return new TableGateway('courses', $dbAdapter, null, $resultSetPrototype);
                },
                'CoursedateTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Coursedate());
                    return new TableGateway('courses_dates', $dbAdapter, null, $resultSetPrototype);
                },
                'InscriptionTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Inscription());
                    return new TableGateway('courses_inscriptions', $dbAdapter, null, $resultSetPrototype);
                },
                'EventTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Event());
                    return new TableGateway('events', $dbAdapter, null, $resultSetPrototype);
                },
                'GraduateTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Graduate());
                    return new TableGateway('graduates', $dbAdapter, null, $resultSetPrototype);
                },
                'TestimonyTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Testimony());
                    return new TableGateway('testimonys', $dbAdapter, null, $resultSetPrototype);
                },
                'CountryTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Country());
                    return new TableGateway('countrys', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }

    public function onLanguageRoute($e)
    {
         // Route has taken place now
        $routeMatch = $e->getRouteMatch();
        $language = $routeMatch->getParam('language', null);

        $available_languages = array(
            'am' => 'am_AM',
			'br' => 'br_BR',
			'da' => 'da_DA',
			'de' => 'de_DE',
			'en' => 'en_Gb',
			'es' => 'es_ES',
			'fr' => 'fr_FR',
			'gr' => 'gr_GR',
			'hi' => 'hi_HI',
			'hr' => 'hr_HR',
			'id' => 'id_ID',
			'il' => 'il_IL',
			'it' => 'it_IT',
			'jp' => 'jp_JP',
			'ko' => 'ko_KO',
			'ms' => 'ms_MS',
			'nl' => 'nl_NL',
			'no' => 'no_NO',
			'ph' => 'ph_PH',
			'pl' => 'pl_PL',
			'pt' => 'pt_PT',
			'ro' => 'ro_RO',
			'ru' => 'ru_RU',
			'sa' => 'sa_SA',
			'se' => 'se_SE',
			'th' => 'th_TH',
			'tr' => 'tr_TR',
			'ua' => 'ua_UA',
			'vn' => 'vn_VN',

        );

        $translator = $e->getApplication()->getServiceManager()->get('translator');

        if ( isset($language) && array_key_exists(strtolower($language), $available_languages) )
        {
            setlocale(LC_ALL, $available_languages[strtolower($language)]);
            $translator->setLocale($available_languages[strtolower($language)]);
        }
        else {
            setlocale(LC_ALL, 'en_US');
            $translator->setLocale('en_Gb');
        }
    }
}
