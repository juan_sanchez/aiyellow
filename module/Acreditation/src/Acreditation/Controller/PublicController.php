<?php 

namespace Acreditation\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Acreditation\Model\Course;
use Acreditation\Model\Coursedate;
use Acreditation\Model\Inscription;
use Acreditation\Model\Event;
use Acreditation\Model\Graduate;
use Acreditation\Model\Testimony;
use Acreditation\Model\Country;

use Acreditation\Form\CourseForm;      
use Acreditation\Form\CoursedateForm;      
use Acreditation\Form\InscriptionForm;      
use Acreditation\Form\EventForm;      
use Acreditation\Form\TestimonyForm;      
use Zend\Paginator\Paginator;

class PublicController extends AbstractActionController
{
    protected $courseTable;
    protected $coursedateTable;
    protected $inscriptionTable;
    protected $eventTable;
    protected $graduateTable;
    protected $testimonyTable;
    protected $countryTable;
    
    ////////////////////GRADUTE ACTIONS////////////////////
    public function indexAction(){
		$languageCfg = $this->getLanguageConfig();
		$this->setLanguageTranslator();
		$this->layout($languageCfg['layout']);

        return new ViewModel(
            array (
                'language' => $languageCfg['language']
            )
        );	
	}
	
	public function graduatepaginatorAction(){
		$this->setLanguageTranslator();
		$this->layout('layout/ajaxLayout');
		$request = $this->getRequest();
		$paginator = null;
		
        if ( $request->isPost() )
        {
			
			$post = $this->getRequest()->getPost()->toArray();
			$paginator = $this->getGraduateTable()->fetchAll(true);
			$paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', $post['page']));
			$paginator->setItemCountPerPage(8);					
		}
       
        return new ViewModel(
            array (
				'language' => $post['language'],
                'paginator' => $paginator
            )
        );
	}
	
	public function graduateAction(){
		$languageCfg = $this->getLanguageConfig();
		$this->setLanguageTranslator();
		$this->layout($languageCfg['layout']);
		
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('public');
        }
        try {
            $graduate = $this->getGraduateTable()->getGraduate($id);
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('public');
        }

		return new ViewModel(
            array (
                'language'  => $languageCfg['language'],
                'graduate'  => $graduate,
            )
        );
	}	
	
	////////////////////COURSE ACTIONS////////////////////
	public function coursesAction(){
		$languageCfg = $this->getLanguageConfig();
		$this->setLanguageTranslator();
		$this->layout($languageCfg['layout']);

        return new ViewModel(
            array (
                'language' => $languageCfg['language']
            )
        );		
	}
	
	public function coursepaginatorAction(){
		$this->setLanguageTranslator();
		$this->layout('layout/ajaxLayout');
		$request = $this->getRequest();
		$paginator = null;
		
        if ( $request->isPost() )
        {
			$post = $this->getRequest()->getPost()->toArray();
			$paginator = $this->getCourseTable()->fetchAll(true);
			$paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', $post['page']));
			$paginator->setItemCountPerPage(6);					
		}
       
        return new ViewModel(
            array (
				'language' => $post['language'],                
				'paginator' => $paginator
            )
        );	
	}
	
	public function courseAction(){
		$languageCfg = $this->getLanguageConfig();
		$this->setLanguageTranslator();
		$this->layout($languageCfg['layout']);
		
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('public');
        }
        try {
            $course = $this->getCourseTable()->getCourse($id);
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('public');
        }

		$courseDates = $this->getCoursedateTable()->fetchAll($id);
		return new ViewModel(
            array (
                'language'  => $languageCfg['language'],
                'course'  => $course,
                'courseDates'  => $courseDates,
            )
        );	
	}

	public function inscriptionmodalAction(){
		$languageCfg = $this->getLanguageConfig();
		$this->setLanguageTranslator();
		$this->layout('layout/ajaxLayout');
		
		$post = $this->getRequest()->getPost()->toArray();
		$id_course = $post['id_course'];
		
		$course = $this->getCourseTable()->getCourse($id_course);
		$courseDates = $this->getCoursedateTable()->fetchAll($id_course);
		
		return new ViewModel(
            array (
                'language'  => $languageCfg['language'],
                'courseDates'  => $courseDates,
                'course'  => $course,
            )
        );		
		
	}
	
	public function submitinscriptionajaxAction(){
       $language = $this->params()->fromRoute('language');
		$request = $this->getRequest();
		$this->layout('layout/ajaxLayout');
		
		//VALIDATE INSCRIPTION FORM		
        if ( $request->isPost() )
        {
			
			$formValid = false;
            $inscription = new Inscription();
            $form = new InscriptionForm();
            $form->setInputFilter($inscription->getInputFilter());
			$form->setData($this->getRequest()->getPost()->toArray());

			if ( $form->isValid() )
			{
				$formValid = true;
				$inscription->exchangeArray($form->getData());
				$this->getInscriptionTable()->saveInscription($inscription);
			}
		}	

		$result = new JsonModel(array(
            'status' => $formValid,
        ));
		
        return $result;		
	}
	
	////////////////////EVENTS ACTIONS////////////////////	
	public function eventsAction(){
		$languageCfg = $this->getLanguageConfig();
		$this->setLanguageTranslator();
		$this->layout($languageCfg['layout']);

        return new ViewModel(
            array (
                'language' => $languageCfg['language']
            )
        );		
	}
	
	public function eventpaginatorAction(){
		$this->setLanguageTranslator();
		$this->layout('layout/ajaxLayout');
		$request = $this->getRequest();
		$paginator = null;
		
        if ( $request->isPost() )
        {
			
			$post = $this->getRequest()->getPost()->toArray();
			$paginator = $this->getEventTable()->fetchAll(true);
			$paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', $post['page']));
			$paginator->setItemCountPerPage(6);					
		}
       
        return new ViewModel(
            array (
				'language' => $post['language'],
                'paginator' => $paginator
            )
        );	
	}
	
	public function eventAction(){
		$languageCfg = $this->getLanguageConfig();
		$this->setLanguageTranslator();
		$this->layout($languageCfg['layout']);
		
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('public');
        }
        try {
            $event = $this->getEventTable()->getEvent($id);
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('public');
        }

		return new ViewModel(
            array (
                'language'  => $languageCfg['language'],
                'event'  => $event,
            )
        );	
	}    
    
    ////////////////////TESTIMONY ACTIONS////////////////////	
    public function testimonysAction(){
		$languageCfg = $this->getLanguageConfig();
		$this->setLanguageTranslator();
		$this->layout($languageCfg['layout']);
		$countrys = $this->getCountryTable()->fetchAll();
		
		return new ViewModel(
            array (
                'language'  => $languageCfg['language'],
                'countrys' => $countrys
            )
        );		
	}
	
	public function testimonypaginatorAction(){
		$this->setLanguageTranslator();
		$this->layout('layout/ajaxLayout');
		$request = $this->getRequest();
		$paginator = null;
		
        if ( $request->isPost() )
        {
			
			$post = $this->getRequest()->getPost()->toArray();
			$paginator = $this->getTestimonyTable()->fetchAll(true, $post['country']);
			$paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', $post['page']));
			$paginator->setItemCountPerPage(6);					
		}
       
        return new ViewModel(
            array (
			    'language' => $post['language'],
                'paginator' => $paginator
            )
        );	
	}	
	
    ////////////////////FAQ ACTIONS////////////////////	
    public function faqAction(){
		$languageCfg = $this->getLanguageConfig();
		$this->setLanguageTranslator();
		$this->layout($languageCfg['layout']);

		return new ViewModel(
            array (
                'language'  => $languageCfg['language'],
            )
        );		
	}
	
	////////////////////CONTACT ACTIONS////////////////////	
	public function contactAction(){
		$languageCfg = $this->getLanguageConfig();
		$this->setLanguageTranslator();
		$this->layout($languageCfg['layout']);

		return new ViewModel(
            array (
                'language'  => $languageCfg['language'],
            )
        );			
	}
	
	////////////////////COMMON FUNCTIONS////////////////////
	public function getLanguageConfig(){
        $language  = $this->params()->fromRoute('language');
		
		if($language == 'il' || $language == 'sa'){	
			$layout = 'layout/rtlLayout';
		}else{
			$layout = 'layout/mainLayout';
        }
        
        return array('layout' => $layout, 'language' => $language);
	}
	
	public function setLanguageTranslator(){
		$translate = $this->getServiceLocator()->get('viewhelpermanager')->get('translate');
	}
	
	////////////////////GET TABLES////////////////////
    public function getCourseTable()
    {
        if (!$this->courseTable) {
            $sm = $this->getServiceLocator();
            $this->courseTable = $sm->get('Acreditation\Model\CourseTable');
        }
        return $this->courseTable;
    }
    
	public function getCoursedateTable()
    {
        if (!$this->coursedateTable) {
            $sm = $this->getServiceLocator();
            $this->coursedateTable = $sm->get('Acreditation\Model\CoursedateTable');
        }
        return $this->coursedateTable;
    }
   
	public function getInscriptionTable()
    {
        if (!$this->inscriptionTable) {
            $sm = $this->getServiceLocator();
            $this->inscriptionTable = $sm->get('Acreditation\Model\InscriptionTable');
        }
        return $this->inscriptionTable;
    }
   
	public function getEventTable()
    {
        if (!$this->eventTable) {
            $sm = $this->getServiceLocator();
            $this->eventTable = $sm->get('Acreditation\Model\EventTable');
        }
        return $this->eventTable;
    }

	public function getGraduateTable()
    {
        if (!$this->graduateTable) {
            $sm = $this->getServiceLocator();
            $this->graduateTable = $sm->get('Acreditation\Model\GraduateTable');
        }
        return $this->graduateTable;
    }
       
	public function getTestimonyTable()
    {
        if (!$this->testimonyTable) {
            $sm = $this->getServiceLocator();
            $this->testimonyTable = $sm->get('Acreditation\Model\TestimonyTable');
        }
        return $this->testimonyTable;
    }
    
	public function getCountryTable()
    {
        if (!$this->countryTable) {
            $sm = $this->getServiceLocator();
            $this->countryTable = $sm->get('Acreditation\Model\CountryTable');
        }
        return $this->countryTable;
    }
}
