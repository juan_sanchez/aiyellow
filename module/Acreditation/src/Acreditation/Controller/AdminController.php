<?php 

namespace Acreditation\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Acreditation\Model\Course;
use Acreditation\Model\Coursedate;
use Acreditation\Model\Inscription;
use Acreditation\Model\Event;
use Acreditation\Model\Graduate;
use Acreditation\Model\Testimony;
use Acreditation\Model\Country;

use Acreditation\Form\CourseForm;      
use Acreditation\Form\CoursedateForm;      
use Acreditation\Form\InscriptionForm;      
use Acreditation\Form\EventForm;      
use Acreditation\Form\GraduateForm;      
use Acreditation\Form\TestimonyForm;      
use Zend\Paginator\Paginator;

class AdminController extends AbstractActionController
{
    protected $courseTable;
    protected $coursedateTable;
    protected $inscriptionTable;
    protected $eventTable;
    protected $graduateTable;
    protected $testimonyTable;
    protected $countryTable;
    
    public function indexAction(){}
    
///////////////////////////COURSE ACTIONS///////////////////////////
    public function courselistAction(){
        $this->layout('layout/adminLayout');
        return new ViewModel(
            array (
                'courses' => $this->getCourseTable()->fetchAll()
            )
        );	
	}
	
    public function courseaddAction(){
		$this->layout('layout/adminLayout');
        $form = new CourseForm();
        $form->get('submit')->setValue('add');

        $request = $this->getRequest();

        if ( $request->isPost() )
        {	
			
            $course = new Course();
            $form->setInputFilter($course->getInputFilter());
			$post = array_merge_recursive(
				$this->getRequest()->getPost()->toArray(),
				$this->getRequest()->getFiles()->toArray()
			);

			$form->setData($post);

			if ( $form->isValid() )
			{
				//Rename the file
				$hash = date('YmdHis');
				move_uploaded_file($post['picture']['tmp_name'], COURSES_PICS_PATH.'/course_'.$hash.'.png');
                $formData = $form->getData();
                if($formData->picture['name'] != ''){
					$formData->picture['name'] = 'course_'.$hash.'.png';
				}	
				$course->exchangeArray($formData);
				$this->getCourseTable()->saveCourse($course);
				return $this->redirect()->toUrl('/admin/courselist');
			}
 
        }
        return array('form' => $form,);	
	}
	
    public function courseeditAction(){
	$this->layout('layout/adminLayout');
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('admin', array(
                'action' => 'courseadd'
            ));
        }
        try {
            $course = $this->getCourseTable()->getCourse($id);
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('admin', array(
                'action' => 'courselist'
            ));
        }

        $form  = new CourseForm();
        $form->bind($course);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($course->getInputFilter());
            
			$post = array_merge_recursive(
				$this->getRequest()->getPost()->toArray(),
				$this->getRequest()->getFiles()->toArray()
			);

			$form->setData($post);
			
            if ($form->isValid()) {
				//Rename the file
		$hash = date('YmdHis');
		move_uploaded_file($post['picture']['tmp_name'], COURSES_PICS_PATH.'/course_'.$hash.'.png');
                $formData = $form->getData();
                if($formData->picture['name'] != ''){
		 $formData->picture['name'] = 'course_'.$hash.'.png';
		}				
                $this->getCourseTable()->saveCourse($formData);
                // Redirect to list of courses
                return $this->redirect()->toUrl('/admin/courselist');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );		
	}
	
    public function coursedeleteAction(){
		$this->layout('layout/adminLayout');
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('course');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getCourseTable()->deleteCourse($id);
                //AGREGAR FUNCION PARA BORRAR INSCRIPTOS Y FECHAS!!!
            }

            return $this->redirect()->toUrl('/admin/courselist');
        }

        return array(
            'id'    => $id,
            'course' => $this->getCourseTable()->getCourse($id)
        );	
	}

///////////////////////////COURSE_DATE ACTIONS///////////////////////////
    public function coursedatepaginatorAction(){
		$this->layout('layout/ajaxLayout');
		$request = $this->getRequest();
		$paginator = array();

        if ( $request->isPost() )
        {
			$post = $this->getRequest()->getPost()->toArray();
			$paginator = $this->getCoursedateTable()->fetchAll($post['id_course'], true);
			$paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', $post['page']));
			$paginator->setItemCountPerPage(4);					
		}
      
        return new ViewModel(
            array (
                'paginator' => $paginator,
                'id_course' => $post['id_course']
            )
        );		
	}
	
    public function dateaddAction(){
		$this->layout('layout/adminLayout');
        $id_course = (int) $this->params()->fromRoute('id', 0);
        $form = new CoursedateForm();
        $form->get('submit')->setValue('add');
        $form->get('id_course')->setValue($id_course);
		
        $request = $this->getRequest();
        if ( $request->isPost() )
        {	

            $coursedate = new Coursedate();
            $form->setInputFilter($coursedate->getInputFilter());
			$post = $this->getRequest()->getPost()->toArray();
			$form->setData($post);
			
			if ( $form->isValid() )
			{
				$formData = $form->getData();
				$coursedate->exchangeArray($formData);
				$this->getCoursedateTable()->saveCoursedate($coursedate);
				return $this->redirect()->toUrl("/admin/courseedit/{$post['id_course']}");
			}
 
        }
        return array(
			'form' => $form,
			'id_course' => $id_course
		);			
	}
	
    public function datesdeletajaxAction(){
		$request = $this->getRequest();
		$this->layout('layout/ajaxLayout');
		$success = false;
		
        if ( $request->isPost() )
        {
			$post = $this->getRequest()->getPost()->toArray();
			$this->getCoursedateTable()->deleteCoursedate($post['id']);
			$success = true;
		}	

		$result = new JsonModel(array(
            'success'=>$success,
        ));
		
        return $result;
	}
    
///////////////////////////COURSE_INSCRIPTION ACTIONS///////////////////////////    
    public function inscriptionpaginatorAction(){
		$this->layout('layout/ajaxLayout');
		$request = $this->getRequest();
		$paginator = array();

        if ( $request->isPost() )
        {
			$post = $this->getRequest()->getPost()->toArray();
			$paginator = $this->getInscriptionTable()->fetchAll($post['id_course'], true);
			$paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', $post['page']));
			$paginator->setItemCountPerPage(4);					
		}
      
        return new ViewModel(
            array (
                'paginator' => $paginator,
                'id_course' => $post['id_course']
            )
        );		
	}
	
    public function inscriptionaddAction(){
		$this->layout('layout/adminLayout');
        $id_course = (int) $this->params()->fromRoute('id', 0);
        
        $dates = $this->getCoursedateTable()->fetchAll($id_course);
        $form = new InscriptionForm();
        $form->get('submit')->setValue('add');
        $form->get('id_course')->setValue($id_course);
		
        $request = $this->getRequest();
        if ( $request->isPost() )
        {	

            $inscription = new Inscription();
            $form->setInputFilter($inscription->getInputFilter());
			$post = $this->getRequest()->getPost()->toArray();

			$form->setData($post);
			
			if ( $form->isValid() )
			{
				$formData = $form->getData();
				$inscription->exchangeArray($formData);
				$this->getInscriptionTable()->saveInscription($inscription);
				return $this->redirect()->toUrl("/admin/courseedit/{$post['id_course']}");
			}
 
        }
        return array(
			'form' => $form,
			'id_course' => $id_course,
			'dates' => $dates
		);			
	}
	
    public function inscriptiondeleteajaxAction(){
		$request = $this->getRequest();
		$this->layout('layout/ajaxLayout');
		$success = false;
		
        if ( $request->isPost() )
        {
			$post = $this->getRequest()->getPost()->toArray();
			$this->getInscriptionTable()->deleteInscription($post['id']);
			$success = true;
		}	

		$result = new JsonModel(array(
            'success'=>$success,
        ));
		
        return $result;
	}
    


///////////////////////////EVENTS ACTIONS///////////////////////////
    public function eventlistAction(){
        $this->layout('layout/adminLayout');
        return new ViewModel(
            array (
                'events' => $this->getEventTable()->fetchAll()
            )
        );	
	}
	
    public function eventaddAction(){
		$this->layout('layout/adminLayout');
        $form = new EventForm();
        $form->get('submit')->setValue('add');

        $request = $this->getRequest();

        if ( $request->isPost() )
        {	
			
            $event = new Event();
            $form->setInputFilter($event->getInputFilter());
			$post = array_merge_recursive(
				$this->getRequest()->getPost()->toArray(),
				$this->getRequest()->getFiles()->toArray()
			);

			$form->setData($post);

			if ( $form->isValid() )
			{
				//Rename the file
				$hash = date('YmdHis');
				move_uploaded_file($post['picture']['tmp_name'], EVENTS_PICS_PATH.'/event_'.$hash.'.png');
                $formData = $form->getData();

				if($formData->picture['name'] != ''){
					$formData->picture['name'] = 'event_'.$hash.'.png';
				}	                
				$event->exchangeArray($formData);
				$this->getEventTable()->saveEvent($event);
				return $this->redirect()->toUrl('/admin/eventlist');
			}
 
        }
        return array('form' => $form,);	
	}
	
    public function eventeditAction(){
		$this->layout('layout/adminLayout');
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('admin', array(
                'action' => 'eventadd'
            ));
        }
        try {
            $event = $this->getEventTable()->getEvent($id);
        }
        catch (\Exception $ex) {
			return $this->redirect()->toUrl('/admin/eventlist');
        }

        $form  = new EventForm();
        $form->bind($event);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($event->getInputFilter());
            
			$post = array_merge_recursive(
				$this->getRequest()->getPost()->toArray(),
				$this->getRequest()->getFiles()->toArray()
			);

			$form->setData($post);
			
            if ($form->isValid()) {
				//Rename the file
				$hash = date('YmdHis');
				move_uploaded_file($post['picture']['tmp_name'], EVENTS_PICS_PATH.'/event_'.$hash.'.png');
                $formData = $form->getData();
				if($formData->picture['name'] != ''){
					$formData->picture['name'] = 'event_'.$hash.'.png';
				}	
                $this->getEventTable()->saveEvent($formData);
                // Redirect to list of events
                return $this->redirect()->toUrl('/admin/eventlist');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );		
	}
	
    public function eventdeleteAction(){
		$this->layout('layout/adminLayout');
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('event');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getEventTable()->deleteEvent($id);
            }

            return $this->redirect()->toUrl('/admin/eventlist');
        }

        return array(
            'id'    => $id,
            'event' => $this->getEventTable()->getEvent($id)
        );	
	}

///////////////////////////GRADUATES ACTIONS///////////////////////////
    public function graduatelistAction(){
        $this->layout('layout/adminLayout');
        return new ViewModel(
            array (
                'graduates' => $this->getGraduateTable()->fetchAll()
            )
        );	
	}
	
    public function graduateaddAction(){
		$this->layout('layout/adminLayout');
        $form = new GraduateForm();
        $form->get('submit')->setValue('add');

        $request = $this->getRequest();

        if ( $request->isPost() )
        {	
			
            $graduate = new Graduate();
            $form->setInputFilter($graduate->getInputFilter());
			$post = array_merge_recursive(
				$this->getRequest()->getPost()->toArray(),
				$this->getRequest()->getFiles()->toArray()
			);

			$form->setData($post);

			if ( $form->isValid() )
			{
				//Rename the file
				$hash = date('YmdHis');
				move_uploaded_file($post['picture']['tmp_name'], COURSES_PICS_PATH.'/graduate_'.$hash.'.png');
                $formData = $form->getData();
				if($formData->picture['name'] != ''){
					$formData->picture['name'] = 'graduate_'.$hash.'.png';
				}	                
				$graduate->exchangeArray($formData);
				$this->getGraduateTable()->saveGraduate($graduate);
				return $this->redirect()->toUrl('/admin/graduatelist');
			}
 
        }
        return array('form' => $form,);	
	}
	
    public function graduateeditAction(){
		$this->layout('layout/adminLayout');
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('admin', array(
                'action' => 'graduateadd'
            ));
        }
        try {
            $graduate = $this->getGraduateTable()->getGraduate($id);
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('graduate', array(
                'action' => 'index'
            ));
        }

        $form  = new GraduateForm();
        $form->bind($graduate);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($graduate->getInputFilter());
            
			$post = array_merge_recursive(
				$this->getRequest()->getPost()->toArray(),
				$this->getRequest()->getFiles()->toArray()
			);

			$form->setData($post);
			
            if ($form->isValid()) {
				//Rename the file
				$hash = date('YmdHis');
				move_uploaded_file($post['picture']['tmp_name'], GRADUATES_PICS_PATH.'/graduate_'.$hash.'.png');
                $formData = $form->getData();
				if($formData->picture['name'] != ''){
					$formData->picture['name'] = 'graduate_'.$hash.'.png';
				}	 
                $this->getGraduateTable()->saveGraduate($formData);
                // Redirect to list of graduates
                return $this->redirect()->toUrl('/admin/graduatelist');
            }
        }

        return array(
            'form' => $form,
			'id' => $id,
        );		
	}
	
    public function graduatedeleteAction(){
		$this->layout('layout/adminLayout');
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('graduate');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getGraduateTable()->deleteGraduate($id);
                //AGREGAR FUNCION PARA BORRAR INSCRIPTOS Y FECHAS!!!
            }

            return $this->redirect()->toUrl('/admin/graduatelist');
        }

        return array(
            'id'    => $id,
            'graduate' => $this->getGraduateTable()->getGraduate($id)
        );	
	}
    
///////////////////////////TESTIMONYS ACTIONS///////////////////////////
    public function testimonylistAction(){
        $this->layout('layout/adminLayout');
        return new ViewModel(
            array (
                'testimonys' => $this->getTestimonyTable()->fetchAll()
            )
        );	
	}
	
    public function testimonyaddAction(){
		$this->layout('layout/adminLayout');
        $form = new TestimonyForm();
        $form->get('submit')->setValue('add');
		$countrys = $this->getCountryTable()->fetchAll();
        $request = $this->getRequest();

        if ( $request->isPost() )
        {	
			
            $testimony = new Testimony();
            $form->setInputFilter($testimony->getInputFilter());
			$post = $this->getRequest()->getPost()->toArray();
			$form->setData($post);

			if ( $form->isValid() )
			{
                $formData = $form->getData();
				$testimony->exchangeArray($formData);
				$this->getTestimonyTable()->saveTestimony($testimony);
				return $this->redirect()->toUrl('/admin/testimonylist');
			}
 
        }
        return array('form' => $form,
					 'countrys' => $countrys,	
					);	
	}
	
    public function testimonyeditAction(){
		$this->layout('layout/adminLayout');
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('action', array(
                'action' => 'testimonyadd'
            ));
        }
        try {
            $testimony = $this->getTestimonyTable()->getTestimony($id);
        }
        catch (\Exception $ex) {
			return $this->redirect()->toUrl('/admin/testimonylist');
        }
		
		$countrys = $this->getCountryTable()->fetchAll();
        $form  = new TestimonyForm();
        $form->bind($testimony);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($testimony->getInputFilter());
			$post = $this->getRequest()->getPost()->toArray();
			
			$form->setData($post);
            if ($form->isValid()) {
				$formData = $form->getData();
                $this->getTestimonyTable()->saveTestimony($formData);
                return $this->redirect()->toUrl('/admin/testimonylist');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
            'countrys' => $countrys,
            'selectedCountry' => $testimony->country
        );		
	}
	
    public function testimonydeleteAction(){
		$this->layout('layout/adminLayout');
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('testimony');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getTestimonyTable()->deleteTestimony($id);
            }

            return $this->redirect()->toUrl('/admin/testimonylist');
        }

        return array(
            'id'    => $id,
            'testimony' => $this->getTestimonyTable()->getTestimony($id)
        );	
	}
    
	////////////////////GET TABLES////////////////////
    public function getCourseTable()
    {
        if (!$this->courseTable) {
            $sm = $this->getServiceLocator();
            $this->courseTable = $sm->get('Acreditation\Model\CourseTable');
        }
        return $this->courseTable;
    }
    
	public function getCoursedateTable()
    {
        if (!$this->coursedateTable) {
            $sm = $this->getServiceLocator();
            $this->coursedateTable = $sm->get('Acreditation\Model\CoursedateTable');
        }
        return $this->coursedateTable;
    }
   
	public function getInscriptionTable()
    {
        if (!$this->inscriptionTable) {
            $sm = $this->getServiceLocator();
            $this->inscriptionTable = $sm->get('Acreditation\Model\InscriptionTable');
        }
        return $this->inscriptionTable;
    }
   
	public function getEventTable()
    {
        if (!$this->eventTable) {
            $sm = $this->getServiceLocator();
            $this->eventTable = $sm->get('Acreditation\Model\EventTable');
        }
        return $this->eventTable;
    }
   
	public function getGraduateTable()
    {
        if (!$this->graduateTable) {
            $sm = $this->getServiceLocator();
            $this->graduateTable = $sm->get('Acreditation\Model\GraduateTable');
        }
        return $this->graduateTable;
    }
    
	public function getTestimonyTable()
    {
        if (!$this->testimonyTable) {
            $sm = $this->getServiceLocator();
            $this->testimonyTable = $sm->get('Acreditation\Model\TestimonyTable');
        }
        return $this->testimonyTable;
    }
    
	public function getCountryTable()
    {
        if (!$this->countryTable) {
            $sm = $this->getServiceLocator();
            $this->countryTable = $sm->get('Acreditation\Model\CountryTable');
        }
        return $this->countryTable;
    }
}
