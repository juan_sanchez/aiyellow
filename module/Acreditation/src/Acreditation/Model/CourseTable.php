<?php
namespace Acreditation\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class CourseTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated=false)
    {
        if($paginated) {
            $select = new Select('courses');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Course());
            $paginatorAdapter = new DbSelect(
                $select,
                $this->tableGateway->getAdapter(),
                $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getCourse($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveCourse(Course $course)
    {
        $data = array( 
			'title_EN'        => $course->title_EN,
			'title_ES'        => $course->title_ES,
			'title_PR'        => $course->title_PR,
			'introduction_EN' => $course->introduction_EN,
			'introduction_ES' => $course->introduction_ES,
			'introduction_PR' => $course->introduction_PR,
			'description_EN'  => $course->description_EN,
			'description_ES'  => $course->description_ES,
			'description_PR'  => $course->description_PR,
			'picture'         => $course->picture['name'],
			'place'           => $course->place
		);
		
		if($data['picture'] == ''){ unset($data['picture']); }
        $id = (int)$course->id;

        if ($id == 0)
            $this->tableGateway->insert($data);

        else {
            if ( $this->getCourse($id) )
                $this->tableGateway->update($data, array('id' => $id));

            else throw new \Exception('Form id does not exist');
        }
    }

    public function deleteCourse($id)
    {
        $this->tableGateway->delete(array('id' => $id));
    }
}
