<?php
namespace Acreditation\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\FileInput;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;


class Graduate implements InputFilterAwareInterface 
{
	public $id;      
	public $name;    
	public $country; 
	public $picture; 
	public $email; 
	public $facebook; 
	public $twitter; 
	public $skype; 
	public $gplus; 
	public $data_EN;   
	public $data_ES;    
	public $data_PR;     
	
    protected $inputFilter; 

    public function exchangeArray($data)
    {
        $this->id       = (isset($data['id']))      ? $data['id']      : null;
		$this->name     = (isset($data['name']))    ? $data['name'] 	  : null;
		$this->country  = (isset($data['country'])) ? $data['country'] : null;
		$this->picture  = (isset($data['picture'])) ? $data['picture'] : null;
		$this->email    = (isset($data['email'])) ? $data['email'] : null;
		$this->facebook = (isset($data['facebook'])) ? $data['facebook'] : null;
		$this->twitter  = (isset($data['twitter'])) ? $data['twitter'] : null;
		$this->skype    = (isset($data['skype'])) ? $data['skype'] : null;
		$this->gplus    = (isset($data['gplus'])) ? $data['gplus'] : null;
		$this->data_EN  = (isset($data['data_EN'])) ? $data['data_EN'] : null;
		$this->data_ES  = (isset($data['data_ES'])) ? $data['data_ES'] : null;
		$this->data_PR  = (isset($data['data_PR'])) ? $data['data_PR'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'name',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 200,
						),
					),	
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'country',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 200,
						),
					),	
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'email',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
                        'name'    => 'EmailAddress',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 5,
                            'max'      => 255,
						),
					),	
                ),
            )));

		   $inputFilter->add($factory->createInput(array(
                'name'     => 'facebook',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 200,
						),
					),	
                ),
            )));

		   $inputFilter->add($factory->createInput(array(
                'name'     => 'twitter',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 200,
						),
					),	
                ),
            )));

		   $inputFilter->add($factory->createInput(array(
                'name'     => 'skype',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 200,
						),
					),	
                ),
            )));

		   $inputFilter->add($factory->createInput(array(
                'name'     => 'gplus',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 200,
						),
					),	
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'data_EN',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 0,
                            'max'      => 1000,
                        ),
                    ),
                ),
            )));
           
            $inputFilter->add($factory->createInput(array(
                'name'     => 'data_ES',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 0,
                            'max'      => 1000,
                        ),
                    ),
                ),
            )));
           
            $inputFilter->add($factory->createInput(array(
                'name'     => 'data_PR',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 0,
                            'max'      => 1000,
                        ),
                    ),
                ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
