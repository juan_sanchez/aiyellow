<?php
namespace Acreditation\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class CoursedateTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($id_course, $paginated=false)
    {
        if($paginated) {
            $select = new Select('courses_dates');
            $select->where( "id_course = {$id_course}");
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Coursedate());
            $paginatorAdapter = new DbSelect(
                $select,
                $this->tableGateway->getAdapter(),
                $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        
        $resultSet = $this->tableGateway->select(array('id_course' => $id_course));
        return $resultSet;
    }

    public function getCoursedate($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveCoursedate(Coursedate $coursedate)
    {
        $data = array(
			'id_course' => $coursedate->id_course,
			'date'  	=> $coursedate->date,
			'time'  	=> $coursedate->time
        );

        $id = (int)$coursedate->id;

        if ($id == 0)
            $this->tableGateway->insert($data);

        else {
            if ( $this->getCoursedate($id) )
                $this->tableGateway->update($data, array('id' => $id));

            else throw new \Exception('Form id does not exist');
        }
    }

    public function deleteCoursedate($id)
    {
        $this->tableGateway->delete(array('id' => $id));
    }
}
