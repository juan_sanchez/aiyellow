<?php
namespace Acreditation\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\FileInput;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;


class Event implements InputFilterAwareInterface 
{
	public $id;
	public $title_EN;
	public $title_ES;
	public $title_PR;
	public $introduction_EN;
	public $introduction_ES;
	public $introduction_PR;
	public $description_EN;
	public $description_ES;
	public $description_PR;
	public $picture;
	public $date;
	public $time;
	public $place;
	
    protected $inputFilter; 

    public function exchangeArray($data)
    {
		$this->id               = (isset($data['id'])) ? $data['id'] : null;
		$this->title_EN  		= (isset($data['title_EN'])) ? $data['title_EN'] : null;
		$this->title_ES  		= (isset($data['title_ES'])) ? $data['title_ES'] : null;
		$this->title_PR  		= (isset($data['title_PR'])) ? $data['title_PR'] : null;
		$this->introduction_EN  = (isset($data['introduction_EN'])) ? $data['introduction_EN'] : null;
		$this->introduction_ES  = (isset($data['introduction_ES'])) ? $data['introduction_ES'] : null;
		$this->introduction_PR  = (isset($data['introduction_PR'])) ? $data['introduction_PR'] : null;
		$this->description_EN   = (isset($data['description_EN'])) ? $data['description_EN'] : null;
		$this->description_ES   = (isset($data['description_ES'])) ? $data['description_ES'] : null;
		$this->description_PR   = (isset($data['description_PR'])) ? $data['description_PR'] : null;
		$this->picture          = (isset($data['picture'])) ? $data['picture'] : null;
		$this->date             = (isset($data['date'])) ? $data['date'] : null;
		$this->time             = (isset($data['time'])) ? $data['time'] : null;
		$this->place            = (isset($data['place'])) ? $data['place'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
      if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'title_EN',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 200,
						),
					),	
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'title_ES',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 200,
						),
					),	
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'title_PR',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 200,
						),
					),	
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'introduction_EN',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 0,
                            'max'      => 1000,
                        ),
                    ),
                ),
            )));
           
            $inputFilter->add($factory->createInput(array(
                'name'     => 'introduction_ES',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 0,
                            'max'      => 1000,
                        ),
                    ),
                ),
            )));
           
            $inputFilter->add($factory->createInput(array(
                'name'     => 'introduction_PR',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 0,
                            'max'      => 1000,
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'description_EN',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 0,
                            'max'      => 1000,
                        ),
                    ),
                ),
            )));
           
            $inputFilter->add($factory->createInput(array(
                'name'     => 'description_ES',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 0,
                            'max'      => 1000,
                        ),
                    ),
                ),
            )));
           
            $inputFilter->add($factory->createInput(array(
                'name'     => 'description_PR',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 0,
                            'max'      => 1000,
                        ),
                    ),
                ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name'     => 'date',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
						'name'=>'Date',
						'options'=>array(
							'format'=>'Y-m-d',
							'messages'=>array(
								'dateFalseFormat'=>'Invalid date format, must be mm-dd-yyy', 
								'dateInvalidDate'=>'Invalid date, must be mm-dd-yyy'
							),
						),      
					),	
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'time',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
						'name'=>'Date',
						'options'=>array(
							'format'=>'H:i:s',
							'messages'=>array(
								'dateFalseFormat'=>'Invalid date format, must be h:m:s', 
								'dateInvalidDate'=>'Invalid date, must be h:m:s'
							),
						),      
					),	
                ),
            )));
            
            $inputFilter->add($factory->createInput(array(
                'name'     => 'place',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 500,
						),
					),	
                ),
            )));
            
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
