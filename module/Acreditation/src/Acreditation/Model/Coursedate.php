<?php
namespace Acreditation\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\FileInput;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;


class Coursedate implements InputFilterAwareInterface 
{
	public $id;
	public $id_course;
	public $date;
	public $time;
	
    protected $inputFilter; 

    public function exchangeArray($data)
    {
		$this->id        = (isset($data['id'])) ? $data['id'] : null;
		$this->id_course = (isset($data['id_course'])) ? $data['id_course'] : null;
		$this->date      = (isset($data['date'])) ? $data['date'] : null;
		$this->time      = (isset($data['time'])) ? $data['time'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'id_course',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'date',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
						'name'=>'Date',
						'options'=>array(
							'format'=>'Y-m-d',
							'messages'=>array(
								'dateFalseFormat'=>'Invalid date format, must be mm-dd-yyy', 
								'dateInvalidDate'=>'Invalid date, must be mm-dd-yyy'
							),
						),      
					),	
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'time',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
						'name'=>'Date',
						'options'=>array(
							'format'=>'H:i:s',
							'messages'=>array(
								'dateFalseFormat'=>'Invalid date format, must be h:m:s', 
								'dateInvalidDate'=>'Invalid date, must be h:m:s'
							),
						),      
					),	
                ),
            )));
            
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
