<?php
namespace Acreditation\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class GraduateTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated=false)
    {
        if($paginated) {
            $select = new Select('graduates');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Graduate());
            $paginatorAdapter = new DbSelect(
                $select,
                $this->tableGateway->getAdapter(),
                $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getGraduate($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveGraduate(Graduate $graduate)
    {
        $data = array(
			'name'     => $graduate->name,
			'country'  => $graduate->country,
			'picture'  => $graduate->picture['name'],
			'email'    => $graduate->email,
			'facebook' => $graduate->facebook,
			'twitter'  => $graduate->twitter,
			'skype'    => $graduate->skype,
			'gplus'    => $graduate->gplus,
			'data_EN'  => $graduate->data_EN,
			'data_ES'  => $graduate->data_ES,
			'data_PR'  => $graduate->data_PR
        );
		
		if($data['picture'] == ''){ unset($data['picture']); }
        $id = (int)$graduate->id;

        if ($id == 0)
            $this->tableGateway->insert($data);

        else {
            if ( $this->getGraduate($id) )
                $this->tableGateway->update($data, array('id' => $id));

            else throw new \Exception('Form id does not exist');
        }
    }

    public function deleteGraduate($id)
    {
        $this->tableGateway->delete(array('id' => $id));
    }
}
