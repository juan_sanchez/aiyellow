<?php
namespace Acreditation\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\FileInput;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;


class Country implements InputFilterAwareInterface 
{
	public $country;
	public $code;
	
    protected $inputFilter; 

    public function exchangeArray($data)
    {
		$this->country 		  = (isset($data['country'])) ? $data['country'] : null;
		$this->code			  = (isset($data['code'])) ? $data['code'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
      if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
         
            $inputFilter->add($factory->createInput(array(
                'name'     => 'country',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 200,
						),
					),	
                ),
            )));
         
            $inputFilter->add($factory->createInput(array(
                'name'     => 'code',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 20,
						),
					),	
                ),
            )));
                       
            
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
