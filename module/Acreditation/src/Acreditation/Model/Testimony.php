<?php
namespace Acreditation\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\FileInput;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;


class Testimony implements InputFilterAwareInterface 
{
	public $id;
	public $id_distributor;
	public $name;
	public $country;
	public $rate;
	public $description_EN;
	public $description_ES;
	public $description_PR;
	public $date;
	
    protected $inputFilter; 

    public function exchangeArray($data)
    {
		$this->id             = (isset($data['id'])) ? $data['id'] : null;
		$this->id_distributor = (isset($data['id_distributor'])) ? $data['id_distributor'] : null;
		$this->name			  = (isset($data['name'])) ? $data['name'] : null;
		$this->country 		  = (isset($data['country'])) ? $data['country'] : null;
		$this->rate 		  = (isset($data['rate'])) ? $data['rate'] : null;
		$this->description_EN = (isset($data['description_EN'])) ? $data['description_EN'] : null;
		$this->description_ES = (isset($data['description_ES'])) ? $data['description_ES'] : null;
		$this->description_PR = (isset($data['description_PR'])) ? $data['description_PR'] : null;
		$this->date           = (isset($data['date'])) ? $data['date'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
      if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));
            
            $inputFilter->add($factory->createInput(array(
                'name'     => 'id_distributor',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));
            
			$inputFilter->add($factory->createInput(array(
                'name'     => 'name',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 200,
						),
					),	
                ),
            )));
            
            $inputFilter->add($factory->createInput(array(
                'name'     => 'country',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 200,
						),
					),	
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'rate',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'description_EN',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 0,
                            'max'      => 1000,
                        ),
                    ),
                ),
            )));
            
            $inputFilter->add($factory->createInput(array(
                'name'     => 'description_ES',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 0,
                            'max'      => 1000,
                        ),
                    ),
                ),
            )));
           
            $inputFilter->add($factory->createInput(array(
                'name'     => 'description_PR',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 0,
                            'max'      => 1000,
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'date',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					array(
						'name'=>'Date',
						'options'=>array(
							'format'=>'Y-m-d',
							'messages'=>array(
								'dateFalseFormat'=>'Invalid date format, must be mm-dd-yyy', 
								'dateInvalidDate'=>'Invalid date, must be mm-dd-yyy'
							),
						),      
					),	
                ),
            )));
            
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
