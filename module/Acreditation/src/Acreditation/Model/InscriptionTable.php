<?php
namespace Acreditation\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class InscriptionTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($id_course, $paginated=false)
    {
        if($paginated) {
            $select = new Select('courses_inscriptions');
            $select->where( "id_course = {$id_course}");
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Inscription());
            $paginatorAdapter = new DbSelect(
                $select,
                $this->tableGateway->getAdapter(),
                $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getInscription($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function validateDuplicate($date, $email, $id_course)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id_course' => $id_course, 'email' => $email, 'date' => $date ));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveInscription(Inscription $inscription)
    {
        $data = array(
			'id_course' => $inscription->id_course,
			'name'      => $inscription->name,
			'lastname'  => $inscription->lastname,
			'email'     => $inscription->email,
			'date'      => $inscription->date
        );
		
        $id = (int)$inscription->id;

        if ($id == 0)
            $this->tableGateway->insert($data);

        else {
            if ( $this->getInscription($id) )
                $this->tableGateway->update($data, array('id' => $id));

            else throw new \Exception('Form id does not exist');
        }
    }

    public function deleteInscription($id)
    {
        $this->tableGateway->delete(array('id' => $id));
    }
}
