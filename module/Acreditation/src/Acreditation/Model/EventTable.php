<?php
namespace Acreditation\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class EventTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated=false)
    {
        if($paginated) {
            $select = new Select('events');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Event());
            $paginatorAdapter = new DbSelect(
                $select,
                $this->tableGateway->getAdapter(),
                $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getEvent($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveEvent(Event $event)
    {
        $data = array( 
			'title_EN'        => $event->title_EN,
			'title_ES'        => $event->title_ES,
			'title_PR'        => $event->title_PR,
			'introduction_EN' => $event->introduction_EN,
			'introduction_ES' => $event->introduction_ES,
			'introduction_PR' => $event->introduction_PR,
			'description_EN'  => $event->description_EN,
			'description_ES'  => $event->description_ES,
			'description_PR'  => $event->description_PR,
			'picture'         => $event->picture['name'],
			'date'	          => $event->date,
			'time'  	      => $event->time,
			'place'           => $event->place
		);

		if($data['picture'] == ''){ unset($data['picture']); }
        $id = (int)$event->id;

        if ($id == 0)
            $this->tableGateway->insert($data);

        else {
            if ( $this->getEvent($id) )
                $this->tableGateway->update($data, array('id' => $id));

            else throw new \Exception('Form id does not exist');
        }
    }

    public function deleteEvent($id)
    {
        $this->tableGateway->delete(array('id' => $id));
    }
}
