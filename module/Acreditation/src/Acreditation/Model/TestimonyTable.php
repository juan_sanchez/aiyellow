<?php
namespace Acreditation\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class TestimonyTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated=false, $country)
    {
        if($paginated) {
            $select = new Select('testimonys');
            if($country != 'none'){
				$select->where( "country = '{$country}'");
			}
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Testimony());
            $paginatorAdapter = new DbSelect(
                $select,
                $this->tableGateway->getAdapter(),
                $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function getTestimony($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveTestimony(Testimony $testimony)
    {
        $data = array(
			'id' 			 => $testimony->id,
			'id_distributor' => $testimony->id_distributor,
			'name' 			 => $testimony->name,
			'country' 		 => $testimony->country,
			'rate' 			 => $testimony->rate,
			'description_EN' => $testimony->description_EN,
			'description_ES' => $testimony->description_ES,
			'description_PR' => $testimony->description_PR,
			'date'           => $testimony->date,
        );

        $id = (int)$testimony->id;

        if ($id == 0)
            $this->tableGateway->insert($data);

        else {
            if ( $this->getTestimony($id) )
                $this->tableGateway->update($data, array('id' => $id));

            else throw new \Exception('Form id does not exist');
        }
    }

    public function deleteTestimony($id)
    {
        $this->tableGateway->delete(array('id' => $id));
    }
}
