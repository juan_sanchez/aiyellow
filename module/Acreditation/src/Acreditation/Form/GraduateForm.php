<?php
namespace Acreditation\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class GraduateForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('graduate');
        $this->setAttribute('method', 'post');
  	
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
				'label' => 'Name'
            ),
        ));

        $this->add(array(
            'name' => 'country',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
				'label' => 'Country'
            ),
        ));

		$file = new Element\File('picture');
        $file->setLabel('Picture')
             ->setAttribute('id', 'picture')
             ->setAttribute('class', 'picture');
        $this->add($file);

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
				'label' => 'Email'
            ),
        ));

        $this->add(array(
            'name' => 'facebook',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
				'label' => 'Facebook'
            ),
        ));

        $this->add(array(
            'name' => 'twitter',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
				'label' => 'Twitter'
            ),
        ));

        $this->add(array(
            'name' => 'skype',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
				'label' => 'Skype'
            ),
        ));

        $this->add(array(
            'name' => 'gplus',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
				'label' => 'Google+'
            ),
        ));

        $this->add(array(
            'name' => 'data_EN',
            'attributes' => array(
                'type'  => 'textarea',
            ),
            'options' => array(
				'label' => 'Info (EN)'
            ),
        ));

        $this->add(array(
            'name' => 'data_ES',
            'attributes' => array(
                'type'  => 'textarea',
            ),
            'options' => array(
				'label' => 'Info (ES)'
            ),
        ));

        $this->add(array(
            'name' => 'data_PR',
            'attributes' => array(
                'type'  => 'textarea',
            ),
            'options' => array(
				'label' => 'Info (PR)'
            ),
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
}
