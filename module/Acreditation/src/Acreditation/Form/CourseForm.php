<?php
namespace Acreditation\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class CourseForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('course');
        $this->setAttribute('method', 'post');
  	
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'title_EN',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
				'label' => 'Title (EN)'
            ),
        ));

        $this->add(array(
            'name' => 'title_ES',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
				'label' => 'Title (ES)'
            ),
        ));

        $this->add(array(
            'name' => 'title_PR',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
				'label' => 'Title (PR)'
            ),
        ));

        $this->add(array(
            'name' => 'place',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
				'label' => 'Place'
            ),
        ));

		$file = new Element\File('picture');
        $file->setLabel('Picture')
             ->setAttribute('id', 'picture')
             ->setAttribute('class', 'picture');
        $this->add($file);

        $this->add(array(
            'name' => 'introduction_EN',
            'attributes' => array(
                'type'  => 'textarea',
            ),
            'options' => array(
				'label' => 'Introduction (EN)'
            ),
        ));
        
        $this->add(array(
            'name' => 'introduction_ES',
            'attributes' => array(
                'type'  => 'textarea',
            ),
            'options' => array(
				'label' => 'Introduction (ES)'
            ),
        ));

        $this->add(array(
            'name' => 'introduction_PR',
            'attributes' => array(
                'type'  => 'textarea',
            ),
            'options' => array(
				'label' => 'Introduction (PR)'
            ),
        ));
                
        $this->add(array(
            'name' => 'description_EN',
            'attributes' => array(
                'type'  => 'textarea',
                'class' => 'richtextbox',
            ),
            'options' => array(
				'label' => 'Description (EN)'
            ),
        ));
        
        $this->add(array(
            'name' => 'description_ES',
            'attributes' => array(
                'type'  => 'textarea',
                'class' => 'richtextbox',
            ),
            'options' => array(
				'label' => 'Description (ES)'
            ),
        ));

        $this->add(array(
            'name' => 'description_PR',
            'attributes' => array(
                'type'  => 'textarea',
                'class' => 'richtextbox',
            ),
            'options' => array(
				'label' => 'Description (PR)'
            ),
        ));
	
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
}
