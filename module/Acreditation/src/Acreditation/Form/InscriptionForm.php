<?php
namespace Acreditation\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class InscriptionForm extends Form 
{
	
    public function __construct($name = null)
    {   
        // we want to ignore the name passed
        parent::__construct('inscription');
        $this->setAttribute('method', 'post');
		
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
  	
        $this->add(array(
            'name' => 'id_course',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
				'label' => 'Name'
            ),
        ));

        $this->add(array(
            'name' => 'lastname',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
				'label' => 'Lastname'
            ),
        ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
				'label' => 'Email'
            ),
        ));
                
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
    
}
