<?php
namespace Acreditation\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class TestimonyForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('employee');
        $this->setAttribute('method', 'post');
  	
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'id_distributor',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
            ),
			'options' => array(
				'label' => 'Full name'
            ),
        ));
        
        $this->add(array(
            'name' => 'country',
            'attributes' => array(
                'type'  => 'text',
            ),
			'options' => array(
				'label' => 'country'
            ),
        ));
        
        $this->add(array(
            'type'  => 'Zend\Form\Element\Select',
            'name' => 'rate',
			'options' => array(
				'label' => 'Rate',
				 'value_options' => array(
					 '0' => '0 stars',
					 '1' => '1 stars',
					 '2' => '2 stars',
					 '3' => '3 stars',
					 '4' => '4 stars',
					 '5' => '5 stars',
				  ),
            ),
        ));
		
        $this->add(array(
            'name' => 'description_EN',
            'attributes' => array(
                'type'  => 'textarea',
            ),
            'options' => array(
				'label' => 'Description (EN)'
            ),
        ));
        
        $this->add(array(
            'name' => 'description_ES',
            'attributes' => array(
                'type'  => 'textarea',
            ),
            'options' => array(
				'label' => 'Description (ES)'
            ),
        ));

        $this->add(array(
            'name' => 'description_PR',
            'attributes' => array(
                'type'  => 'textarea',
            ),
            'options' => array(
				'label' => 'Description (PR)'
            ),
        ));

		$this->add(array(
			'type' => 'Zend\Form\Element\Date',
			'name' => 'date',
			'options' => array(
				 'label' => 'Date'
			),
			'attributes' => array(
				 'min' => '2013-01-01',
				 'max' => '2020-01-01',
				 'class' => 'datepicker',
			)
		));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
}
