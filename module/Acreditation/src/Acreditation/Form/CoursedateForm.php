<?php
namespace Acreditation\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class CoursedateForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('coursedate');
        $this->setAttribute('method', 'post');
  	
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
  	
        $this->add(array(
            'name' => 'id_course',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));

		$this->add(array(
			'type' => 'Zend\Form\Element\Date',
			'name' => 'date',
			'options' => array(
				 'label' => 'Date'
			),
			'attributes' => array(
				 'min' => '2013-01-01',
				 'max' => '2020-01-01',
				 'class' => 'datepicker',
			)
		));

		$this->add(array(
			'type' => 'Zend\Form\Element\Time',
			'name' => 'time',
			'options' => array(
				 'label' => 'Time'
			),
			'attributes' => array(
				'class' => 'timepicker',
			)
		));
	 
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
}
