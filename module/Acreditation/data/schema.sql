-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1.natty~ppa.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 27-03-2013 a las 03:59:31
-- Versión del servidor: 5.1.63
-- Versión de PHP: 5.3.5-1ubuntu7.11

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `aiyellow`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `country` varchar(100) CHARACTER SET utf8 NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `picture` varchar(200) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `skype` varchar(255) DEFAULT NULL,
  `gplus` varchar(255) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_ES` text CHARACTER SET utf8,
  `data_EN` text CHARACTER SET utf8,
  `data_PR` text CHARACTER SET utf8,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Volcado de datos para la tabla `employee`
--

INSERT INTO `employee` (`id`, `country`, `name`, `picture`, `email`, `facebook`, `twitter`, `skype`, `gplus`, `timestamp`, `data_ES`, `data_EN`, `data_PR`) VALUES
(1, 'Polonia', 'Goyeneche', '/avatar_20130327015436.png', 'mail@mail.com', '', '', '', '', '2013-03-27 04:54:36', 'Este es un texto en español...', 'Este es un texto en ingles...', 'Este es un texto en portuges...'),
(2, 'Argentina', 'Juan M. Sanchez', 'avatar_20130325032438.png', NULL, NULL, NULL, NULL, NULL, '2013-03-25 06:25:31', 'Este es un texto en español...', 'Este es un texto en ingles...', 'Este es un texto en portuges...'),
(29, 'Deutschland', 'Nina Hagen', 'avatar_20130325032419.png', NULL, NULL, NULL, NULL, NULL, '2013-03-25 06:25:36', 'Nina Hagen was born in the former East Berlin, East Germany, the daughter of Hans Hagen (also known as Hans Oliva-Hagen), a scriptwriter, and Eva-Maria Hagen (née Buchholz), an actress and singer. Her paternal grandfather died in the Sachsenhausen concentration camp (her paternal grandparents were Jewish).[1] Her parents divorced when she was two years old, and growing up she saw her father infrequently. At age four, she began to study ballet, and was considered an opera prodigy by the time she was nine.', 'Nina Hagen was born in the former East Berlin, East Germany, the daughter of Hans Hagen (also known as Hans Oliva-Hagen), a scriptwriter, and Eva-Maria Hagen (née Buchholz), an actress and singer. Her paternal grandfather died in the Sachsenhausen concentration camp (her paternal grandparents were Jewish).[1] Her parents divorced when she was two years old, and growing up she saw her father infrequently. At age four, she began to study ballet, and was considered an opera prodigy by the time she was nine.', 'Nina Hagen was born in the former East Berlin, East Germany, the daughter of Hans Hagen (also known as Hans Oliva-Hagen), a scriptwriter, and Eva-Maria Hagen (née Buchholz), an actress and singer. Her paternal grandfather died in the Sachsenhausen concentration camp (her paternal grandparents were Jewish).[1] Her parents divorced when she was two years old, and growing up she saw her father infrequently. At age four, she began to study ballet, and was considered an opera prodigy by the time she was nine.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `employee_comments`
--

CREATE TABLE IF NOT EXISTS `employee_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_employee` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `email` varchar(200) CHARACTER SET utf8 NOT NULL,
  `comment` text CHARACTER SET utf8 NOT NULL,
  `status` tinyint(4) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Volcado de datos para la tabla `employee_comments`
--

INSERT INTO `employee_comments` (`id`, `id_employee`, `name`, `email`, `comment`, `status`, `timestamp`) VALUES
(1, 1, 'Jhon Doe', 'desconocido@anonimo.com', 'Este es un comentario de prueba...', 1, '2013-03-23 05:22:39'),
(32, 1, 'Juan de los palotes', 'usuario.jm@gmail.com', 'A ver si esto anda....', 1, '2013-03-24 22:47:59'),
(33, 1, 'nombre', 'email@mail.com', 'Comentario', 1, '2013-03-24 23:31:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `display_name` varchar(50) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `state` smallint(6) DEFAULT NULL,
  `role` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`user_id`, `username`, `email`, `display_name`, `password`, `state`, `role`) VALUES
(2, NULL, 'admin@aiyellow.development.com', NULL, '$2y$14$GeF4zFLkZ9hExIG.Uclk/uUcEoahVjAZsi90Fli65YFDdRb6LtukG', NULL, 'admin'),
(3, NULL, 'admin@admin.com', NULL, '$2a$14$e0LDTmlhdWxa..srrBY4l./YS6oB/8RP6XAZlxZj9qksawGPFb01m', NULL, 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
