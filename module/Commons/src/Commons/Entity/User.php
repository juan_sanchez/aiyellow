<?php

namespace Commons\Entity;

use ZfcUser\Entity\User as ZfcUserEntity ;

class User extends ZfcUserEntity
{
    /**
     * @var string
     */
    protected $role ;

    /**
     * Set role.
     *
     * @param string $role
     * @return UserInterface
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * Get role.
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }
}
