<?php

namespace Commons\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin,
    Zend\Session\Container as SessionContainer,
    Zend\Permissions\Acl\Acl,
    Zend\Permissions\Acl\Role\GenericRole as Role,
    Zend\Permissions\Acl\Resource\GenericResource as Resource;

class AuthPlugin extends AbstractPlugin
{
    protected $sesscontainer;

    private function getSessContainer()
    {
        if (!$this->sesscontainer) {
            $this->sesscontainer = new SessionContainer('zftutorial');
        }
        return $this->sesscontainer;
    }

    private function getAcl()
    {
        //setting ACL...
        $acl = new Acl();
        //add role ..
        $acl->addRole(new Role('anonymous'));
        $acl->addRole(new Role('admin'));

        $acl
            ->addResource(new Resource('admin'))
            ->addResource(new Resource('public'))
            ->addResource(new Resource('zfcuser')) ;

        $acl->deny();
        $acl->allow('admin');

        $acl->allow('anonymous','zfcuser');
        $acl->allow('anonymous','public');

        return $acl ;
    }

    public function doAuthorization($e)
    {
        $acl = $this->getAcl() ;

        $controller = $e->getRouteMatch()->getParam('controller');
        $action     = $e->getRouteMatch()->getParam('action');

        $aController     = $e->getTarget();
        $controllerClass = get_class($aController);
        $namespace       = substr($controllerClass, 0, strpos($controllerClass, '\\'));

        $sm = $e->getApplication()->getServiceManager();
        $auth = $sm->get('zfcuser_auth_service');

        $role = ( ! $auth->hasIdentity() ) ? 'anonymous' : $auth->getIdentity()->getRole() ;

        if( ! $acl->isAllowed($role,$controller,$action) && $namespace !== 'ZfcUser' )
        {
            $router = $e->getRouter();
            $url = $router->assemble(array(), array('name' => 'zfcuser'));

            $response = $e->getResponse();
            $response->setStatusCode(302);
            //redirect to login route...
            /* change with header('location: '.$url); if code below not working */
            $response->getHeaders()->addHeaderLine('Location', $url);
            $e->stopPropagation();
        }
    }
}
