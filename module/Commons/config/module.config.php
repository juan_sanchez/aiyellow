<?php

return array(
    'controller_plugins' => array(
        'invokables' => array(
            'AuthPlugin' => 'Commons\Controller\Plugin\AuthPlugin',
        )
    )
);
