-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 25, 2013 at 12:15 AM
-- Server version: 5.5.29
-- PHP Version: 5.3.10-1ubuntu3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `aiyellow_accreditations`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_EN` varchar(200) NOT NULL,
  `title_ES` varchar(200) NOT NULL,
  `title_PR` varchar(200) NOT NULL,
  `introduction_EN` text NOT NULL,
  `introduction_ES` text NOT NULL,
  `introduction_PR` text NOT NULL,
  `description_EN` text NOT NULL,
  `description_ES` text NOT NULL,
  `description_PR` text NOT NULL,
  `picture` varchar(500) NOT NULL,
  `place` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `title_EN`, `title_ES`, `title_PR`, `introduction_EN`, `introduction_ES`, `introduction_PR`, `description_EN`, `description_ES`, `description_PR`, `picture`, `place`) VALUES
(9, 'Title', 'Titulo', 'O Titulo', 'This is an example of introduction.', 'Este es un ejemplo de introduccion.', 'Este é um exemplo de introdução.', '<p>This is an example of description.</p>\r\n<ul>\r\n<li>1</li>\r\n<li>2</li>\r\n<li>3</li>\r\n</ul>', '<p>Este es un ejemplo de descripcion.</p>\r\n<ul>\r\n<li>1</li>\r\n<li>2</li>\r\n<li>3</li>\r\n</ul>', '<p>Este &eacute; um exemplo de descri&ccedil;&atilde;o.</p>\r\n<ul>\r\n<li>1</li>\r\n<li>2</li>\r\n<li>3</li>\r\n</ul>', 'course_20130424215132.png', 'Somewhere Over The Rainbow');

-- --------------------------------------------------------

--
-- Table structure for table `courses_dates`
--

CREATE TABLE IF NOT EXISTS `courses_dates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_course` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `courses_dates`
--

INSERT INTO `courses_dates` (`id`, `id_course`, `date`, `time`) VALUES
(1, 2, '2013-04-21', '12:00:00'),
(2, 2, '2013-04-22', '12:00:00'),
(3, 2, '2013-04-03', '09:38:00'),
(4, 2, '2013-04-02', '07:00:00'),
(5, 4, '2013-04-03', '07:00:00'),
(6, 4, '2013-04-03', '01:00:00'),
(7, 4, '2013-04-03', '12:00:00'),
(8, 4, '2013-04-18', '01:00:00'),
(9, 4, '2013-04-26', '01:09:00'),
(10, 4, '2013-04-26', '01:09:00'),
(11, 4, '2013-04-26', '01:09:00'),
(12, 4, '2013-04-03', '01:29:00'),
(16, 2, '2013-04-03', '12:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `courses_inscriptions`
--

CREATE TABLE IF NOT EXISTS `courses_inscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_course` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `courses_inscriptions`
--

INSERT INTO `courses_inscriptions` (`id`, `id_course`, `name`, `lastname`, `email`, `date`) VALUES
(1, 2, 'Juan', 'Sanchez', 'mail@mail.com', '0000-00-00'),
(3, 4, 'Juan', 'Sanchez', 'mail@mail.com', '0000-00-00'),
(4, 3, 'Juan', 'Sanchez', 'usuarios.jm@gmail.com', '0000-00-00'),
(5, 3, 'Juan', 'Sanchez', 'usuarios.jm@gmail.com', '0000-00-00'),
(6, 3, 'Goyeneches', 'Sanchez', 'usuarios.jm@gmail.com', '0000-00-00'),
(7, 3, 'aaaa', 'Sanchez', 'mail@mail.com', '0000-00-00'),
(8, 3, 'aaaa', 'Sanchez', 'usuarios.jm@gmail.com', '0000-00-00'),
(9, 3, 'aaaa', 'Sanchez', 'usuarios.jm@gmail.com', '0000-00-00'),
(10, 3, 'aaaa', 'Sanchez', 'usuarios.jm@gmail.com', '0000-00-00'),
(11, 0, 'Goyenechesxxx', 'Sanchez', 'usuarios.jm@gmail.com', '2013-04-21'),
(12, 0, 'Goyenechesxxx', 'Sanchez', 'usuarios.jm@gmail.com', '2013-04-21'),
(13, 2, 'NUEVO', 'dddd', 'usuarios.jm@gmail.com', '2013-04-21'),
(14, 2, 'dddd', 'as', 'asd@mail.com', '2013-04-21'),
(15, 3, 'Juan', 'dddd', 'usuarios.jm@gmail.com', '2013-04-21');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_EN` varchar(200) NOT NULL,
  `title_ES` varchar(200) NOT NULL,
  `title_PR` varchar(200) NOT NULL,
  `introduction_EN` text NOT NULL,
  `introduction_ES` text NOT NULL,
  `introduction_PR` text NOT NULL,
  `description_EN` text NOT NULL,
  `description_ES` text NOT NULL,
  `description_PR` text NOT NULL,
  `picture` varchar(500) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `place` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `title_EN`, `title_ES`, `title_PR`, `introduction_EN`, `introduction_ES`, `introduction_PR`, `description_EN`, `description_ES`, `description_PR`, `picture`, `date`, `time`, `place`) VALUES
(4, 'Title', 'Titulo', 'O Titulo', 'This is an example of introduction.', 'Este es un ejemplo de introduccion.', 'Este é um exemplo de introdução.', '<p>This is an example of description.</p>\r\n<ul>\r\n<li>1</li>\r\n<li>2</li>\r\n<li>3</li>\r\n</ul>', '<p>Este es un ejemplo de descripcion.</p>\r\n<ul>\r\n<li>1</li>\r\n<li>2</li>\r\n<li>3</li>\r\n</ul>', '<p>Este &eacute; um exemplo de descri&ccedil;&atilde;o.</p>\r\n<ul>\r\n<li>1</li>\r\n<li>2</li>\r\n<li>3</li>\r\n</ul>', 'event_20130424225047.png', '2013-04-23', '03:33:00', 'Somewhere Over The Rainbow');

-- --------------------------------------------------------

--
-- Table structure for table `graduates`
--

CREATE TABLE IF NOT EXISTS `graduates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `country` varchar(100) CHARACTER SET utf8 NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `picture` varchar(200) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `skype` varchar(255) DEFAULT NULL,
  `gplus` varchar(255) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_ES` text CHARACTER SET utf8,
  `data_EN` text CHARACTER SET utf8,
  `data_PR` text CHARACTER SET utf8,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `graduates`
--

INSERT INTO `graduates` (`id`, `country`, `name`, `picture`, `email`, `facebook`, `twitter`, `skype`, `gplus`, `timestamp`, `data_ES`, `data_EN`, `data_PR`) VALUES
(1, 'Polonia', 'Goyeneches', '/graduate_20130422010257.png', 'mail@mail.com', 'https://www.facebook.com/juanchezx?ref=tn_tnmn', '', '', '', '2013-04-22 04:02:57', 'Este es un texto en español...', 'Este es un texto en ingles...', 'Este es un texto en portuges...'),
(2, 'Argentina', 'Juan M. Sanchez', 'avatar_20130325032438.png', NULL, NULL, NULL, NULL, NULL, '2013-03-25 09:25:31', 'Este es un texto en español...', 'Este es un texto en ingles...', 'Este es un texto en portuges...'),
(29, 'Deutschland', 'Nina Hagen', 'avatar_20130325032419.png', NULL, NULL, NULL, NULL, NULL, '2013-03-25 09:25:36', 'Nina Hagen was born in the former East Berlin, East Germany, the daughter of Hans Hagen (also known as Hans Oliva-Hagen), a scriptwriter, and Eva-Maria Hagen (née Buchholz), an actress and singer. Her paternal grandfather died in the Sachsenhausen concentration camp (her paternal grandparents were Jewish).[1] Her parents divorced when she was two years old, and growing up she saw her father infrequently. At age four, she began to study ballet, and was considered an opera prodigy by the time she was nine.', 'Nina Hagen was born in the former East Berlin, East Germany, the daughter of Hans Hagen (also known as Hans Oliva-Hagen), a scriptwriter, and Eva-Maria Hagen (née Buchholz), an actress and singer. Her paternal grandfather died in the Sachsenhausen concentration camp (her paternal grandparents were Jewish).[1] Her parents divorced when she was two years old, and growing up she saw her father infrequently. At age four, she began to study ballet, and was considered an opera prodigy by the time she was nine.', 'Nina Hagen was born in the former East Berlin, East Germany, the daughter of Hans Hagen (also known as Hans Oliva-Hagen), a scriptwriter, and Eva-Maria Hagen (née Buchholz), an actress and singer. Her paternal grandfather died in the Sachsenhausen concentration camp (her paternal grandparents were Jewish).[1] Her parents divorced when she was two years old, and growing up she saw her father infrequently. At age four, she began to study ballet, and was considered an opera prodigy by the time she was nine.'),
(30, 'dssdds', 'aaaa', '/graduate_20130422010309.png', 'asd@mail.com', '', '', '', '', '2013-04-22 04:03:09', 'asd', 'asd', 'asd'),
(31, 'Polonia', 'Goyeneches', '/graduate_20130422010257.png', 'mail@mail.com', 'https://www.facebook.com/juanchezx?ref=tn_tnmn', '', '', '', '2013-04-22 04:02:57', 'Este es un texto en español...', 'Este es un texto en ingles...', 'Este es un texto en portuges...'),
(32, 'dssdds', 'aaaa', '/graduate_20130422010309.png', 'asd@mail.com', '', '', '', '', '2013-04-22 04:03:09', 'asd', 'asd', 'asd'),
(33, 'Argentina', 'Juan M. Sanchez', 'avatar_20130325032438.png', NULL, NULL, NULL, NULL, NULL, '2013-03-25 09:25:31', 'Este es un texto en español...', 'Este es un texto en ingles...', 'Este es un texto en portuges...'),
(34, 'Polonia', 'Goyeneches', '/graduate_20130422010257.png', 'mail@mail.com', 'https://www.facebook.com/juanchezx?ref=tn_tnmn', '', '', '', '2013-04-22 04:02:57', 'Este es un texto en español...', 'Este es un texto en ingles...', 'Este es un texto en portuges...'),
(36, 'dssdds', 'aaaa', '/graduate_20130422010309.png', 'asd@mail.com', '', '', '', '', '2013-04-22 04:03:09', 'asd', 'asd', 'asd');

-- --------------------------------------------------------

--
-- Table structure for table `testimonys`
--

CREATE TABLE IF NOT EXISTS `testimonys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_distributor` int(11) NOT NULL,
  `description_EN` text NOT NULL,
  `description_ES` text NOT NULL,
  `description_PR` text NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `testimonys`
--

INSERT INTO `testimonys` (`id`, `id_distributor`, `description_EN`, `description_ES`, `description_PR`, `date`) VALUES
(1, 1, 'asd', 'asd', 'ads', '2013-04-30');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `display_name` varchar(50) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `state` smallint(6) DEFAULT NULL,
  `role` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `email`, `display_name`, `password`, `state`, `role`) VALUES
(2, NULL, 'admin@aiyellow.development.com', NULL, '$2y$14$GeF4zFLkZ9hExIG.Uclk/uUcEoahVjAZsi90Fli65YFDdRb6LtukG', NULL, 'admin'),
(3, NULL, 'admin@admin.com', NULL, '$2a$14$e0LDTmlhdWxa..srrBY4l./YS6oB/8RP6XAZlxZj9qksawGPFb01m', NULL, 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
